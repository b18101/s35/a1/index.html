
const express = require('express');
const router = express.Router();

const userControllers = require('../controllers/userControllers');
console.log(userControllers);

// create user route
router.post('/', userControllers.createUserControllers);

// get all user route
router.get('/', userControllers.getAllUsersController);


// -----------------------ACTIVITY #2--------------------------------


// update user's username
router.put('/:id', userControllers.updateUserUsernameController);


// get single usere
router.get('/getSingleUser/:id', userControllers.getSingleUserController);



module.exports = router;

