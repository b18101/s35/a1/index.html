const mongoose = require('mongoose');

//Schema - blueprint for our data/ document

const taskSchema = new mongoose.Schema({

	name: String,

	status: String
});

//Mongoose Model
/*
	mongoose.model(<nameOfCollectionInAtlas>, <schemaToFollow>)

*/

module.exports = mongoose.model("Task", taskSchema);